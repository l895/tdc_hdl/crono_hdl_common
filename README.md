# Crono HDL Common

This repository have some common verilog sources used among Crono TDC Core design and Crono Signal Generator.

To make a modular design and not repeat code twice the common source code is in this repository.

## Testing

We are using `cocotb` to run the testbenchs of Verilog modules. The testbenchs with this framework can be done in Python! yeeeii!

See `cocotb` doc: https://docs.cocotb.org/en/stable/ for more information. 

We make a docker image with cocotb prepared (see `.gci/Dockerfile`). In the CI pipeline, the image is built and then the testbenchs
are executed inside this image. But, we can build this image locally and execute the testbenchs.

### In CI

The gitlab repository has configured (see `gitlab-ci.yml`) a pipeline to run the testbenchs in each commit.

### Locally

We can enter in the `dockershell.sh` and execute the script located in `src/tests/run_tests.sh` to run the testbenchs. Also, this scripts allows to run individual tests.

### Code styling

The code styling standard followed was the one described in SystemVerilog website: https://www.systemverilog.io/styleguide

