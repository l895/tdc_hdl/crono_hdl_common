
import cocotb
from cocotb.triggers import Timer
import sys
import os
from cocotb.clock import Clock

# add parent directory to sys path (to import common python files!)
sys.path.append(os.path.dirname(os.path.dirname(os.path.realpath(__file__))))
from lock_arbiter_common import generate_data_test, reset_dut

REQUESTERS_QTY=5

@cocotb.test()
async def test_all_combinations_small_hold_time_no_rest(dut):
    """
    Test the arbiter with some sequence of requesters input.
    This test does not leave rest betwen cases to the DUT, instead, presents
    the data of the next case right after the previous end.

    Note: In the `requesters_sequence` two consecutive inputs
    do not must to be high the granted requester bit. This test
    simulates that in each step the granted requester finish with the resource.
    Exammple:
        requesters_sequence[1] = 0b00001
        requesters_sequence[2] = 0b10001
        will give problemas because the test will assert if requester 0 (bit 0) do not 
        release the resource after it iteration.
    """
    requesters_sequence = [
         0b11111,
         0b00001,
         0b01110,
         0b00111,
         0b01011,
         0b00101,
         0b01010,
         0b00110,
         0b00001,
         0b10000,
         0b01000,
         0b00111,
         0b10011,
         0b00001,
    ]
    test_cases = generate_data_test(requesters_sequence, requesters_qty=REQUESTERS_QTY)
    clock_period_ns = 10
    hold_time_ns = clock_period_ns * 2
    clock = Clock(dut.i_clk, clock_period_ns, units="ns")  # Create a 10us period clock on port clk
    cocotb.start_soon(clock.start())  # Start the clock

    dut.i_requesters.value = 0

    await reset_dut(dut.i_reset, clock_period_ns, 'ns')

    await Timer(clock_period_ns*2, units="ns")

    for case in test_cases:
        dut._log.info('requesters: 0b{:08b}'.format(case['input']))
        # one requester will take the resource in the next clock cycle
        dut.i_requesters.value = case['input']
        dut._log.info('requester {} will hold resource for {} ns'.format(case['expected_encoded_output'], hold_time_ns))
        # NOTE: the DUT have a response time of 2 clock cycles until sets the new grant
        # Here I leave 2 clock cycles and a small amount of time more
        await Timer(clock_period_ns*2, units="ns")
        await Timer(round(clock_period_ns/5), units="ns")

        assert dut.o_grants_encoded.value.integer == case['expected_encoded_output'], 'encoded output is not the expected! {:d} != {:d}'.format(dut.o_grants_encoded.value.integer, case['expected_encoded_output'])
        assert dut.o_grants_unencoded.value.integer == case['expected_unencoded_output'], 'unencoded output is not the expected! {:d} != {:d}'.format(dut.o_grants_unencoded.value.integer, case['expected_encoded_output'])
        await Timer(hold_time_ns, units="ns")
        dut._log.info('requester {} releases the resource!'.format(case['expected_encoded_output']))


@cocotb.test()
async def test_all_combinations_small_hold_time_rest(dut):
    """
    Test the arbiter with some sequence of requesters input.
    This test leaves some rest betwen cases to the DUT. 

    Note: In the `requesters_sequence` two consecutive inputs
    do not must to be high the granted requester bit. This test
    simulates that in each step the granted requester finish with the resource.
    Exammple:
        requesters_sequence[1] = 0b00001
        requesters_sequence[2] = 0b10001
        will give problemas because the test will assert if requester 0 (bit 0) do not 
        release the resource after it iteration.
    """
    requesters_sequence = [
         0b11111,
         0b00001,
         0b01110,
         0b00111,
         0b01011,
         0b00101,
         0b01010,
         0b00110,
         0b00001,
         0b10000,
         0b01000,
         0b00111,
         0b10011,
         0b00001,
    ]
    requesters_sequence_data = generate_data_test(requesters_sequence, requesters_qty=REQUESTERS_QTY)
    clock_period_ns = 10
    hold_time_ns = clock_period_ns * 2

    ## Each test case have different rest times and hold times
    test_cases = [
        {'rest_time_ns': clock_period_ns    , 'hold_time_ns': clock_period_ns    },
        {'rest_time_ns': clock_period_ns    , 'hold_time_ns': clock_period_ns*2  },
        {'rest_time_ns': clock_period_ns    , 'hold_time_ns': clock_period_ns*3  },
        {'rest_time_ns': clock_period_ns    , 'hold_time_ns': clock_period_ns*10 },

        {'rest_time_ns': clock_period_ns*2  , 'hold_time_ns': clock_period_ns    },
        {'rest_time_ns': clock_period_ns*2  , 'hold_time_ns': clock_period_ns*2  },
        {'rest_time_ns': clock_period_ns*2  , 'hold_time_ns': clock_period_ns*3  },
        {'rest_time_ns': clock_period_ns*2  , 'hold_time_ns': clock_period_ns*10 },

        {'rest_time_ns': clock_period_ns*3  , 'hold_time_ns': clock_period_ns    },
        {'rest_time_ns': clock_period_ns*3  , 'hold_time_ns': clock_period_ns*2  },
        {'rest_time_ns': clock_period_ns*3  , 'hold_time_ns': clock_period_ns*3  },
        {'rest_time_ns': clock_period_ns*3  , 'hold_time_ns': clock_period_ns*10 },

        {'rest_time_ns': clock_period_ns*10 , 'hold_time_ns': clock_period_ns    },
        {'rest_time_ns': clock_period_ns*10 , 'hold_time_ns': clock_period_ns*2  },
        {'rest_time_ns': clock_period_ns*10 , 'hold_time_ns': clock_period_ns*3  },
        {'rest_time_ns': clock_period_ns*10 , 'hold_time_ns': clock_period_ns*10 },

    ]

    clock = Clock(dut.i_clk, clock_period_ns, units="ns")  # Create a 10us period clock on port clk
    cocotb.start_soon(clock.start())  # Start the clock

    dut.i_requesters.value = 0

    await reset_dut(dut.i_reset, clock_period_ns, 'ns')

    await Timer(clock_period_ns*2, units="ns")

    for case in test_cases:
        dut._log.info('beginning test with {} ns of rest time between multiple steps'.format(case['rest_time_ns']))
        for step in requesters_sequence_data:
            dut._log.info('requesters: 0b{:08b}'.format(step['input']))
            # one requester will take the resource in the next clock cycle
            dut.i_requesters.value = step['input']
            dut._log.info('requester {} will hold resource for {} ns'.format(step['expected_encoded_output'], case['hold_time_ns']))

            # NOTE: the DUT have a response time of 2 clock cycles until sets the new grant
            # Here I leave 2 clock cycles and a small amount of time more
            await Timer(clock_period_ns*2, units="ns")
            await Timer(round(clock_period_ns/5), units="ns")

            assert dut.o_grants_encoded.value.integer == step['expected_encoded_output'], 'encoded output is not the expected! {:d} != {:d}'.format(dut.o_grants_encoded.value.integer, step['expected_encoded_output'])
            assert dut.o_grants_unencoded.value.integer == step['expected_unencoded_output'], 'unencoded output is not the expected! {:d} != {:d}'.format(dut.o_grants_unencoded.value.integer, step['expected_encoded_output'])
            await Timer(case['hold_time_ns'], units="ns")
            dut._log.info('requester {} releases the resource!'.format(step['expected_encoded_output']))

            dut.i_requesters.value = 0
            dut._log.info('leaving {} ns of rest...'.format(case['rest_time_ns']))
            await Timer(case['rest_time_ns'], 'ns')