from cocotb.triggers import Timer

async def reset_dut(sync_reset, duration, time_unit):
    """
    Put in reset state the DUT for a certain time interval 
    and unreset it then.

    Parameters
    ----------
    sync_reset : cocotb.handle.ModifiableObject
        The reset signal of the DUT.

    duration : int
        The width of the reset pulse.

    time_unit: str
        The time unit of the duration parameter (ns, us, etc...)
    """
    sync_reset._log.info("Touching the reset button of the DUT! ({}{})".format(duration, time_unit))
    sync_reset.value = 1
    await Timer(duration, units=time_unit)
    sync_reset.value = 0
    sync_reset._log.info("The reset was completed!")


def get_msb_set(number:int) -> int: 
    ''' Return the most significant bit set of a number, counting from 0 to N-1.
    Where N is the quantity of bits of the number. Takes as MSB the righmost bit.
    '''
    msb = 0 
    for i in range(number.bit_length()-1,-1,-1): 
        analyzed_bit = (number >> i) & 0x1 
        if analyzed_bit: 
            msb = i 
            break 
    return msb 


def generate_data_test(requesters_sequence: list, requesters_qty:int) -> list:
    '''
    Takes several possible requesters inputs and generate a data test with that.

    Will retrive a list with dictionaries of this format:
        {'input': 0b111111, 'expected_encoded_output': 0b100000, 'expected_unencoded_output': 5},

    Where:
    * 'input' is the input that must be set to the arbiter,
    * 'expected_encoded_output' is the encoded output that must compute the encoder for the given input
    * 'expected_unencoded_output' is the unencoded output that must compute the encoder for the given input

    Parameters
    ----------
    requesters_sequence: list
        A list with various inputs of the arbiter. Can be taken as a sequence of 
        requesters trying to take the control of the resource. This method will take each
        step of this sequence and then generate a valid data set to test the arbiter. 

    requesters_qty : int  
        The quantity of requesters that will be.

    '''
    data_test = []
    for step in requesters_sequence:
        msb_set = get_msb_set(step)
        data_test.append({'input': step, 'expected_encoded_output': msb_set, 'expected_unencoded_output': 1 << msb_set})
    return data_test