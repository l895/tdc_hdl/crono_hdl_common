
import cocotb
from cocotb.clock import Clock
from cocotb.triggers import RisingEdge, FallingEdge
from cocotb.utils import get_sim_time

from tb_tools.utils import reset_dut


SYSTEM_CLOCK_FREQ_HZ = 100000 # clock freq was set to all the test, the module need it to be defined as a param
TOGGLE_PERIOD_MS = 1000

SYSTEM_CLOCK_PERIOD_US = (1000000/SYSTEM_CLOCK_FREQ_HZ)


@cocotb.test()
async def test_timed_toggler(dut):
    """ Test that the timed toggler toggles the output with a correct frequency. """


    clock = Clock(dut.i_clk, SYSTEM_CLOCK_PERIOD_US, units="us")
    cocotb.start_soon(clock.start())  # Start the clock

    await reset_dut(dut.i_reset, SYSTEM_CLOCK_PERIOD_US*2, 'us')

    dut._log.info('staring test with a clock freq of {} Hz ({} us)'.format(SYSTEM_CLOCK_FREQ_HZ, SYSTEM_CLOCK_PERIOD_US))

    for i in range(3):
        dut._log.info(f"beginning cycle {i} checks!")
        init_time_ms = get_sim_time('ms')
        await RisingEdge(dut.o_toggle_out)
        toggle_time_ms = get_sim_time('ms') - init_time_ms

        assert toggle_time_ms > TOGGLE_PERIOD_MS * 0.98, f"cycle {i}: 1st check: toggled time is bigger than the expected!"
        assert toggle_time_ms < TOGGLE_PERIOD_MS * 1.02, f"cycle {i}: 1st check: toggled time is smaller than the expected!"

        dut._log.info(f"cycle {i}: 1st checks finished! (toggle_time_ms={toggle_time_ms})")

        init_time_ms = get_sim_time('ms')
        await FallingEdge(dut.o_toggle_out)
        toggle_time_ms = get_sim_time('ms') - init_time_ms

        assert toggle_time_ms > TOGGLE_PERIOD_MS * 0.98, f"cycle {i}: 2nd check: toggled time is bigger than the expected!"
        assert toggle_time_ms < TOGGLE_PERIOD_MS * 1.02, f"cycle {i}: 2nd check: toggled time is smaller than the expected!"
        
        dut._log.info(f"cycle {i}: 2nd checks finished! (toggle_time_ms={toggle_time_ms})")

    dut._log.info(f"timed toggler worked well! toggling with a period of {TOGGLE_PERIOD_MS} ms")