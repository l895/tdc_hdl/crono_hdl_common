import cocotb
from cocotb.clock import Clock
from cocotb.triggers import Timer, RisingEdge, with_timeout, Join
from cocotb.utils import get_sim_time
from tb_tools.utils import block_until_signal, reset_dut

CLOCK_PERIOD_NS = 10
FLAGS_QTY = 3
FLAGS_SET_HIGH_TIME_US = 100

@cocotb.test()
async def test_alert_flags_time(dut):
    """ Test if alert flags are set during the correspondent time interval. """

    clock = Clock(dut.i_clk, CLOCK_PERIOD_NS, units="ns")  # Create a 10us period clock on port i_clk
    cocotb.start_soon(clock.start())  # Start the clock

    await Timer(CLOCK_PERIOD_NS*3, units="ns") ## to differentiate different test cases
    
    await reset_dut(dut.i_reset, round(CLOCK_PERIOD_NS*2), "ns")
    await RisingEdge(dut.i_clk)

    dut.i_hit.value = 0

    for flag in range(FLAGS_QTY):
        hit_value = 1 << flag
        dut._log.info(f"/*------CASE {flag}--------*/")
        dut._log.info(f"hit value: 0b{hit_value:03b}")

        dut.i_hit.value = hit_value
        init_time_us = get_sim_time('us')
        wait_until_flag_is_set = cocotb.start_soon(block_until_signal(dut.o_flag, hit_value, CLOCK_PERIOD_NS))
        await with_timeout(Join(wait_until_flag_is_set), timeout_time=FLAGS_SET_HIGH_TIME_US*2, timeout_unit='us')
        alert_time_us = get_sim_time('us') - init_time_us

        assert alert_time_us < FLAGS_SET_HIGH_TIME_US*1.1, f"alert time is bigger than expected! {alert_time_us} > {FLAGS_SET_HIGH_TIME_US*1.1}" 
        assert alert_time_us > FLAGS_SET_HIGH_TIME_US*0.9, f"alert time is smaller than expected! {alert_time_us} < {FLAGS_SET_HIGH_TIME_US*0.9}"
        dut._log.info(f"{FLAGS_SET_HIGH_TIME_US*1.1} > {alert_time_us} > {FLAGS_SET_HIGH_TIME_US*0.9}")