
import cocotb
from cocotb.clock import Clock
from cocotb.triggers import Timer
from cocotb.utils import get_sim_time

SYSTEM_CLOCK_FREQ_MHZ = 10 # clock freq was set to all the test, the module need it to be defined as a param
SYSTEM_CLOCK_PERIOD_NS = round(1000/SYSTEM_CLOCK_FREQ_MHZ)

async def reset_dut(sync_reset, duration, time_unit):
    """
    Put in reset state the DUT for a certain time interval 
    and unreset it then.

    Parameters
    ----------
    sync_reset : cocotb.handle.ModifiableObject
        The reset signal of the DUT.

    duration : int
        The width of the reset pulse.

    time_unit: str
        The time unit of the duration parameter (ns, us, etc...)
    """
    sync_reset._log.info("Touching the reset button of the DUT! ({}{})".format(duration, time_unit))
    sync_reset.value = 1
    await Timer(duration, units=time_unit)
    sync_reset.value = 0
    sync_reset._log.info("The reset was completed!")


@cocotb.test()
async def test_us_timer_counting(dut):
    """ Test that the timer counts up the set microseconds """

    test_cases = [
        {'total_us': 2},
        {'total_us': 10},
        {'total_us': 20},
        {'total_us': 100},
        {'total_us': 159},
        {'total_us': 398},
        {'total_us': 978},
    ]

    clock = Clock(dut.i_clk, SYSTEM_CLOCK_PERIOD_NS, units="ns")  # Create a 10us period clock on port clk
    cocotb.start_soon(clock.start())  # Start the clock

    await reset_dut(dut.i_reset, SYSTEM_CLOCK_PERIOD_NS, 'ns')

    dut._log.info('staring test with a clock freq of {} MHz ({} ns)'.format(SYSTEM_CLOCK_FREQ_MHZ, SYSTEM_CLOCK_PERIOD_NS))
    case_counter = 0
    for case in test_cases:
        # Set microseconds to count first:
        dut.i_time_to_count_we.value = 1
        dut.i_time_to_count_us.value = case['total_us']
        await Timer(SYSTEM_CLOCK_PERIOD_NS, 'ns')
        dut.i_time_to_count_we.value = 0
        # Then reset the timer
        dut.i_reset.value = 1
        await Timer(SYSTEM_CLOCK_PERIOD_NS, 'ns')
        dut.i_reset.value = 0
        init_time = get_sim_time('us')
        dut._log.info('{}: counting {} us'.format(case_counter, case['total_us']))

        while( dut.o_time_reached.value == 0 ):
            await Timer(SYSTEM_CLOCK_PERIOD_NS, 'ns')

        delta_time = round(get_sim_time('us') - init_time)
        assert case['total_us'] == delta_time, 'counted time is not correct! {} us != {} us'.format(case['total_us'], delta_time)
        dut._log.info('{}: o_time_reached port was set at a correct time: {} us!'.format(case_counter, delta_time))

        await Timer(SYSTEM_CLOCK_PERIOD_NS, 'ns')
        case_counter += 1

