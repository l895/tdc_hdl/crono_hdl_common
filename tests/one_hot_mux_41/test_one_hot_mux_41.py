import cocotb
from cocotb.triggers import Timer

INPUT_WIDTH = 19

@cocotb.test()
async def test_one_hot_mux_41_simple(dut):
    """ Test that the input is propagated to the output if it is selected """

    dut.i_data0.value = 0b0001
    dut.i_data1.value = 0b0010
    dut.i_data2.value = 0b0100
    dut.i_data3.value = 0b1000

    for j in [0b0001, 0b0010, 0b0100, 0b1000]:
        dut._log.info(f"selecting signal: {j}")
        dut.i_sel.value = j  # Assign the random value val to the input port d
        await Timer(1, 'ns')
        assert dut.o_data.value == j, f"output was not the expected!"
        await Timer(1, 'ns')