
from signal import signal
from time import CLOCK_THREAD_CPUTIME_ID
import cocotb
from cocotb.triggers import Timer, RisingEdge, FallingEdge
from cocotb.clock import Clock


def setup(dut, clk_period_ns):
    clock = Clock(dut.clk, clk_period_ns, units="ns")
    cocotb.start_soon(clock.start())  # Start the clock


async def fill_ram_with_incremental_data_c2c(   dut, 
                                                top_value:int,
                                                ram_columns:int,
                                                column_width:int):
    """
    Fill the RAM with a incremental data, from 0 to top_value.
    This method will increment a counter and will fill the RAM with the values
    of this counter up to top_value. If the memory is divided in columns
    then will iterate over the columns in each cycle (because this, the
    function is called '_c2c' (column to column)). 
    The RAM is like a matrix and each element is a pair (column, row).
    Each element have it's own address, but the `write_enable` signal
    must enable what column is writed at each time.

    Parameters
    ----------
    dut : 
        The Device Under Test.

    top_value : int
        The quantity of values that the method will write into the RAM.

    ram_columns : int
        The quantity of columns of the RAM.

    column_width : int
        The width (in bits) of each column of the RAM.
    """
    values_left = top_value
    address = 0
    counter = 0

    dut.addr.value = 0
    dut.we.value = 0

    dut._log.info('starting incremental data c2c writing!')
    for i in range(2):
        await RisingEdge(dut.clk)

    while values_left > 0:
        dut.addr.value = address        # in this cycle we will read from this address
        col_vals = ''
        for i in range(0, ram_columns): # write column by column
            dut.we.value = (1 << i)     # write only a certain column
            dut.di.value = (counter << i*column_width)
            await RisingEdge(dut.clk)
            values_left -= 1
            col_vals += ' {:02X}'.format(counter)
            counter += 1
            if values_left <= 0:
                break
            else:
                await FallingEdge(dut.clk)
        dut._log.info('W: addr: {:03} | row: '.format(address) + col_vals)
        address += 1
    dut.we.value = 0 # unset the `write_enable`


async def verify_incremental_data_c2c(  dut,
                                        max_count:int,
                                        ram_columns:int,
                                        column_width:int,
                                        clk_period_ns:int):
    """
    Check if the RAM have incremental values from 0 to max_count
    from a given memory position.

    Parameters
    ----------
    dut : 
        The Device Under Test.

    max_count : int
        The max count of the incremental values that we must to read.

    ram_columns : int
        The quantity of columns of the RAM.

    column_width : int
        The width (in bits) of each column of the RAM.

    clock_period_ns : int
        The clock period in nanoseconds. We need this because we delay a little
        the read after a rising edge of the clocl.
    """

    address = 0
    previous_value = 0
    actual_value = 0
    max_count_reached = False
    values_readed = 0


    dut._log.info('starting incremental data c2c verification!')
    for i in range(2):
        await RisingEdge(dut.clk)

    while max_count_reached == False:
        await FallingEdge(dut.clk)
        dut.addr.value = address
        await RisingEdge(dut.clk)
        # wait a little to read, because we will read the previous value if not
        await Timer(round(clk_period_ns/10), 'ns')
        row_value = dut.dout.value.integer
        col_vals = ''
        for i in range(0, ram_columns):
            col_vals += ' {:02X}'.format( (row_value >> column_width*i) & (2**(column_width)-1) )  
        dut._log.info('R: addr: {:03} | row: '.format(address)+col_vals)

        # verify te values column by column
        for i in range(0, ram_columns):
            previous_value = actual_value
            actual_value = (row_value >> column_width*i) & (2**(column_width)-1)
            dut._log.info('actual_value: {:03}, previous value: {:03}'.format(actual_value, previous_value))
            if actual_value == max_count:
                dut._log.info('reached the max_count! {} == {}'.format(actual_value, max_count))
                max_count_reached = True
                values_readed += 1
                break

            assert actual_value >= previous_value, 'readed value is not incremental! {} < {}'.format(actual_value, previous_value)

        address += 1

        assert values_readed <= max_count, 'something wrong! top value {} not reached but we readed {} elements from RAM!'.format(max_count, values_readed)