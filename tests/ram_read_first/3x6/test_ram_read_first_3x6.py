import cocotb
import os
import sys

# add parent directory to sys path (to import common python files!)
sys.path.append(os.path.dirname(os.path.dirname(os.path.realpath(__file__))))

from ram_read_first_common import setup, fill_ram_with_incremental_data_c2c, verify_incremental_data_c2c


@cocotb.test()
async def test_bram_byte_writing_incremental_pattern_3x6(dut):
    top_value = 0x3F
    clk_period_ns = 10
    ram_columns = 3
    column_width = 6

    setup(dut, clk_period_ns)

    await fill_ram_with_incremental_data_c2c(dut, top_value, ram_columns, column_width)
    # is top_value-1 because we count from 0!
    await verify_incremental_data_c2c(dut, top_value-1, ram_columns, column_width, clk_period_ns)