import random
import cocotb
from cocotb.clock import Clock
from cocotb.triggers import FallingEdge, Timer
from cocotb.binary import BinaryValue

async def reset_dut(sync_reset, duration, time_unit):
    """
    Put in reset state the DUT for a certain time interval 
    and unreset it then.

    Parameters
    ----------
    sync_reset : cocotb.handle.ModifiableObject
        The reset signal of the DUT.

    duration : int
        The width of the reset pulse.

    time_unit: str
        The time unit of the duration parameter (ns, us, etc...)
    """
    sync_reset._log.info("Touching the reset button of the DUT! ({}{})".format(duration, time_unit))
    sync_reset.value = 1
    await Timer(duration, units=time_unit)
    sync_reset.value = 0
    sync_reset._log.info("The reset was completed!")

@cocotb.test()
async def test_reg_n_simple(dut):
    """ Test that D propagates to Q """

    test_data = [1, 14, 26, 95, 100, 24, 11, 67]

    clock = Clock(dut.clk, 10, units="us")  # Create a 10us period clock on port clk
    cocotb.start_soon(clock.start())  # Start the clock

    i = 1
    for data in test_data:
        dut._log.info('setting input D = {}'.format(bin(data)))
        dut.D.value = data  # Assign the random value val to the input port d
        await FallingEdge(dut.clk)
        assert dut.Q.value == data, "output q was incorrect on the {}th cycle".format(i)
        dut._log.info('the output was right! Q = {}'.format(dut.Q.value.binstr))
        i += 1

@cocotb.test()
async def test_reg_n_reset(dut):
    """ Test that D propagates to Q resetting in each cycle"""

    test_data = [1, 14, 26, 95, 100, 24, 11, 67]
    clock_period_us = 10
    clock = Clock(dut.clk, clock_period_us, units="us")
    cocotb.start_soon(clock.start())  # Start the clock
    dut.Q.value = 0

    i = 1
    for data in test_data:
        await reset_dut(dut.sync_reset, 2*clock_period_us, "us")
        assert dut.Q.value == 0, "the dut was reseted correctly!"
        await Timer(clock_period_us, "us")

        dut._log.info('setting input D = {}'.format(bin(data)))
        dut.D.value = data  # Assign the random value val to the input port d
        await Timer(2*clock_period_us, "us")
        assert dut.Q.value == data, "output q was incorrect on the {}th cycle".format(i)
        dut._log.info('the output was right! Q = {}'.format(dut.Q.value.binstr))
        i += 1