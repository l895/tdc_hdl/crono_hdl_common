def get_msb_set(number:int) -> int: 
    ''' Return the most significant bit set of a number, counting from 0 to N-1.
    Where N is the quantity of bits of the number. Takes as MSB the righmost bit.
    '''
    msb = 0 
    for i in range(number.bit_length()-1,-1,-1): 
        analyzed_bit = (number >> i) & 0x1 
        if analyzed_bit: 
            msb = i 
            break 
    return msb 


def generate_data_test(encoder_input_width:int) -> list:
    '''
    Generate the data test with all the possible input combinations to test a
    priority encoder.

    Will retrive a list with dictionaries of this format:
        {'input': 0b111111, 'expected_encoded_output': 0b100000, 'expected_unencoded_output': 5},

    Where:
    * 'input' is the input that must be set to the encoder,
    * 'expected_encoded_output' is the encoded output that must compute the encoder for the given input
    * 'expected_unencoded_output' is the unencoded output that must compute the encoder for the given input

    Parameters
    ----------
    encoder_input_width : 
        The width of the encoder input.
    '''
    data_test = []
    for i in range(2**encoder_input_width): 
        msb_set = get_msb_set(i)
        data_test.append({'input': i, 'expected_encoded_output': msb_set, 'expected_unencoded_output': 1 << msb_set})
    return data_test