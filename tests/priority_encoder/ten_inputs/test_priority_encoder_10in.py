
import cocotb
from cocotb.triggers import Timer
import sys
import os

# add parent directory to sys path (to import common python files!)
sys.path.append(os.path.dirname(os.path.dirname(os.path.realpath(__file__))))
from priority_encoder_common import generate_data_test

INPUT_WIDTH=10

@cocotb.test()
async def test_all_combinations_with_rest(dut):
    """
    Test the priority encoder with every possible inputs combination values 
    and seeing if the output is the expected.
    Here we leaves some rest to the DUT between each case.
    """
    test_cases = generate_data_test(INPUT_WIDTH)

    dut.i_unencoded.value = 0
    await Timer(10, units="ns")

    for case in test_cases:
        dut._log.info('input data: 0b{:08b}'.format(case['input']))
        dut.i_unencoded.value = case['input']

        await Timer(1, units="ns")
        dut.o_encoded.value.integer == case['expected_encoded_output'], 'encoded output is not the expected {} != {}'.format(dut.o_encoded.value.integer, case['expected_encoded_output'])
        dut.o_unencoded.value.integer == case['expected_unencoded_output'], 'unencoded output is not the expected {} != {}'.format(dut.o_unencoded.value.integer, case['expected_unencoded_output'])
        dut._log.info('encoded output: {:10}, and unencoded output: 0b{:08b} are the expected ones!'.format(dut.o_encoded.value.integer, dut.o_unencoded.value.integer))
        await Timer(10, units="ns")

@cocotb.test()
async def test_all_combinations_without_rest(dut):
    """
    Test the priority encoder with every possible inputs combination values 
    and seeing if the output is the expected.
    Here we leaves some rest to the DUT between each case.
    """
    test_cases = generate_data_test(INPUT_WIDTH)

    dut.i_unencoded.value = 0
    await Timer(1, units="ns")

    for case in test_cases:
        dut._log.info('input data: 0b{:08b}'.format(case['input']))
        dut.i_unencoded.value = case['input']

        await Timer(1, units="ns")
        dut.o_encoded.value.integer == case['expected_encoded_output'], 'encoded output is not the expected {} != {}'.format(dut.o_encoded.value.integer, case['expected_encoded_output'])
        dut.o_unencoded.value.integer == case['expected_unencoded_output'], 'unencoded output is not the expected {} != {}'.format(dut.o_unencoded.value.integer, case['expected_unencoded_output'])
        dut._log.info('encoded output: {:10}, and unencoded output: 0b{:08b} are the expected ones!'.format(dut.o_encoded.value.integer, dut.o_unencoded.value.integer))

@cocotb.test()
async def test_active_signal_with_rest(dut):
    """
    Test the priority encoder with every possible inputs combination values 
    and seeing if the output is the expected.
    Here we leaves some rest to the DUT between each case.
    """
    test_cases = generate_data_test(INPUT_WIDTH)

    dut.i_unencoded.value = 0
    await Timer(1, units="ns")

    for case in test_cases:
        dut._log.info('input data: 0b{:08b}'.format(case['input']))
        dut.i_unencoded.value = case['input']

        await Timer(1, units="ns")
        dut.o_encoded.value.integer == case['expected_encoded_output'], 'encoded output is not the expected {} != {}'.format(dut.o_encoded.value.integer, case['expected_encoded_output'])
        dut.o_unencoded.value.integer == case['expected_unencoded_output'], 'unencoded output is not the expected {} != {}'.format(dut.o_unencoded.value.integer, case['expected_unencoded_output'])
        dut._log.info('encoded output: {:10}, and unencoded output: 0b{:08b} are the expected ones!'.format(dut.o_encoded.value.integer, dut.o_unencoded.value.integer))

        await Timer(1, units="ns")
        dut.i_unencoded.value = 0
        dut.o_valid.value.integer == 0, 'valid must be reset when input does not have valid data but is set instead!'
        await Timer(1, units="ns")