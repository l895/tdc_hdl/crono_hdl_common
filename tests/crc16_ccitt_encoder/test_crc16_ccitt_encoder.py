import cocotb
from cocotb.clock import Clock
from cocotb.triggers import FallingEdge, Timer, RisingEdge, with_timeout

from tb_tools.utils import block_until_signal, reset_dut

@cocotb.test()
async def test_crc16_ccitt_encoding(dut):
    """ Test if encoder encodes as expected. """

    data = [ 
        {'in': 0x102F, 'expected_crc': 0xD6FE, 'bytes_qty': 2},
        {'in': 0x1F5F, 'expected_crc': 0xB857, 'bytes_qty': 2},
        {'in': 0x1EADBEEF, 'expected_crc': 0x77F3, 'bytes_qty': 4},
        {'in': 0x155555, 'expected_crc': 0x5389, 'bytes_qty': 3},
        {'in': 0x2F5F, 'expected_crc': 0xBDC2, 'bytes_qty': 2},
        {'in': 0x4F5F, 'expected_crc': 0xB6E8, 'bytes_qty': 2},
        {'in': 0x5F5F, 'expected_crc': 0xB59B, 'bytes_qty': 2},
        {'in': 0x9F5F, 'expected_crc': 0xA3CF, 'bytes_qty': 2},
        {'in': 0xDEADBEEF, 'expected_crc': 0xC457, 'bytes_qty': 4},
        {'in': 0x355555, 'expected_crc': 0xD54F, 'bytes_qty': 3},
        {'in': 0x8AB000001AB5, 'expected_crc': 0x67DA, 'bytes_qty': 6},
        ]

    clock_period_ns = 1
    clock = Clock(dut.clk, clock_period_ns, units="ns")  # Create a 10us period clock on port clk
    cocotb.start_soon(clock.start())  # Start the clock

    await Timer(clock_period_ns*3, units="ns") ## to differentiate different test cases
    
    await reset_dut(dut.i_rst, round(clock_period_ns*1.1), "ns")
    await RisingEdge(dut.clk)

    dut.i_data.value = 0
    dut.i_data_valid.value = 0

    case_counter = 0
    for case in data:
        # the sequence is:
        # 1) set i_data_valid signal to 1
        # 2) wait until next clock cycle
        # 3) insert to the encoder the data bit by bit, starting from MSB and
        # waiting 1 clock cycle between each bit
        # 4) whenever the LSB is inserted set i_data_valid signal to 0
        # 5) wait until o_crc_valid signal is 1
        # 6) read data from o_crc
        dut._log.info(f'\n/** TESTING CASE {case_counter} **/')
        dut._log.info('input to dut: 0x{:X} = 0b{:b}'.format(case['in'],case['in']))
        dut.i_data_valid.value = 1
        await RisingEdge(dut.clk)
        # TODO: icarus verilog does not interprets the previous RisingEdge,
        # for this reason i added another one here. See if we can improve this
        await RisingEdge(dut.clk)

        for i in range(case['bytes_qty']*8):
            # enter bit by bit the data, starting from MSB
            input_bit = (case['in'] >> (case['bytes_qty']*8-1-i)) & 0x1
            #dut._log.info('entering with bit {}'.format(input_bit))
            dut.i_data.value = input_bit
            await RisingEdge(dut.clk)
        dut.i_data_valid.value = 0
        dut.i_data.value = 0

        wait_o_crc_valid_task = cocotb.start_soon(block_until_signal(dut.o_crc_valid, 1, clock_period_ns))
        await with_timeout(wait_o_crc_valid_task, 5, 'us')

        assert dut.o_crc.value == case['expected_crc'], 'CRC is not expected 0x{:04X} (exp) != 0x{:04X} (obtained)'.format(case['expected_crc'], dut.o_crc.value.integer)
        dut._log.info(f"CRC value is the expected! 0x{dut.o_crc.value.integer:04X}")
        
        await Timer(clock_period_ns*2, units="ns") ## to differentiate different test cases
        case_counter += 1