/*
 * Timed flags alert system.
 * This is a flag system that, whenever an input signal is hit, a
 * certain output signal is set high during a time interval.
 *
 * In fact, when an input `i_hit` signal is set high, the correspondent.
 * `o_flag` signal will be high during a time interval.
 *
 * Author: Julian Rodriguez <jnrodriguez@estudiantes.unsam.edu.ar>
 */

module alert_flags(i_reset,i_clk,i_hit,o_flag);
  parameter FLAGS_QTY = 3;
  parameter FLAGS_SET_HIGH_TIME_US = 1000000;
  parameter SYSTEM_CLOCK_FREQUENCY_HZ = 100000000;

  input i_reset;                // synchronous reset (active high)
  input i_clk;                  // input clock
  input [FLAGS_QTY-1:0] i_hit; // event triggers signals
  output [FLAGS_QTY-1:0] o_flag;// flags signals

    genvar i;
    generate
        for(i = 0; i < FLAGS_QTY; i = i + 1) begin: flag
          /* Set led 3 high during 1 seg when a command will be executed. */
          wire w_timer_finished, w_timer_reset;
          assign w_timer_reset = i_reset | i_hit[i];
          
          us_timer #(
              .INITIAL_TIME_TO_COUNT_US(FLAGS_SET_HIGH_TIME_US),
              .CLOCK_FREQUENCY_MHZ(SYSTEM_CLOCK_FREQUENCY_HZ/1000000)
          ) timer (
              .i_clk(i_clk), // input wire clk
              .i_reset(w_timer_reset),
              .i_time_to_count_we(0),                         // signal that enables when the timer will set the `time to count` value
              .i_time_to_count_us(FLAGS_SET_HIGH_TIME_US),    // port used to configure the `time to count` of the timer, unused here
              .o_time_reached(w_timer_finished)
          );
          assign o_flag[i] = ~w_timer_finished;
        end
    endgenerate


/* ILA instantiation if we are debugging this module with Vivado */
//`ifdef DEBUG_CRONO_TDC_LF
//ila_1 ila_debug (
//    .clk(i_clk), // input wire clk
//);
//`endif

// the "macro" to dump signals
`ifdef COCOTB_SIM
initial begin
  $dumpfile ("alert_flags.vcd");
  $dumpvars (0, alert_flags);
  #1;
end
`endif

endmodule
