/*
 * Verilog code of a simple priority encoder.
 * 
 * This priority encoder takes the input bits `i_unencoded` and 
 * only set the most significant bit in the encoded output `o_encoded`.
 * This encoder priorizes only the MSB of the input.
 * This encoder is purely combinational, so no clock is involved.
 *
 * Have two outputs:
 * 1) 'o_encoded' is the number of the most significant bit set in the in input.
 * Counting from 0 to N-1, where N is the width of the input. The bit 0 is the 
 * most rightmost bit.
 * 2) 'o_unencoded' is the most significant bit set in the input but in one-hot format.
 * 
 * Example: 
 * `i_unencoded` = 0b00101101
 * => `o_encoded` = 5 (the bit number 5 is set)
 * => `o_unencoded` = 0b00100000
 *
 * Reference: https://github.com/alexforencich/verilog-axis/blob/master/rtl/priority_encoder.v.
 *
 * Author: Julian Rodriguez <jnrodriguez@estudiantes.unsam.edu.ar>
 */
module priority_encoder(i_unencoded,o_valid,o_encoded,o_unencoded);
    parameter WIDTH = 6;                // Input width

    input  wire [WIDTH-1:0]         i_unencoded;
    output wire                     o_valid;
    output wire [$clog2(WIDTH)-1:0] o_encoded;
    output wire [WIDTH-1:0]         o_unencoded;

    /* Calculate the quantity of levels that needs the encoder.
     * Note: WIDTH must be greater than one always! is the common case.
     * And if not, I prefer to login an error in the compilation/synthesis.
     */
    localparam LEVELS = $clog2(WIDTH); // clog2 rounds up
    localparam PADDED_WIDTH = 2**LEVELS;

// pad input to even power of two
wire [PADDED_WIDTH-1:0] input_padded = {{PADDED_WIDTH-WIDTH{1'b0}}, i_unencoded};

wire [PADDED_WIDTH/2-1:0] stage_valid[LEVELS-1:0];
wire [PADDED_WIDTH/2-1:0] stage_enc[LEVELS-1:0];

generate
    genvar l, n;

    // process input bits; generate valid bit and encoded bit for each pair
    for (n = 0; n < PADDED_WIDTH/2; n = n + 1) begin : loop_in
        assign stage_valid[0][n] = |input_padded[n*2+1:n*2]; // OR reductor operator (|) makes an OR with both elements of this array (this array have only 2 elements)
        assign stage_enc[0][n] = input_padded[n*2+1];
    end

    // compress down to single valid bit and encoded bus
    for (l = 1; l < LEVELS; l = l + 1) begin : loop_levels
        for (n = 0; n < PADDED_WIDTH/(2*2**l); n = n + 1) begin : loop_compress
            assign stage_valid[l][n] = |stage_valid[l-1][n*2+1:n*2];
            assign stage_enc[l][(n+1)*(l+1)-1:n*(l+1)] = stage_valid[l-1][n*2+1] ? {1'b1, stage_enc[l-1][(n*2+2)*l-1:(n*2+1)*l]} : {1'b0, stage_enc[l-1][(n*2+1)*l-1:(n*2)*l]};
        end
    end
endgenerate

assign o_valid = stage_valid[LEVELS-1];
assign o_encoded = stage_enc[LEVELS-1];
assign o_unencoded = 1 << o_encoded;


/* Desktop test with WIDTH = 5, LSB_HIGH_PRIORITY = 0
 *
 * input  wire [4:0]    i_unencoded;
 * output wire          o_valid;
 * output wire [2:0]    o_encoded;
 * output wire [4:0]    o_unencoded;
 *
 * localparam LEVELS = 3; // clog2 rounds up
 * localparam PADDED_WIDTH = 8;
 * 
 * wire [7:0] input_padded = {{3{1'b0}}, i_unencoded};
 * 
 * wire [3:0] stage_valid[2:0];
 * wire [3:0] stage_enc[2:0];
 *
 * // process input bits; generate valid bit and encoded bit for each pair
 * assign stage_valid[0][0] = |input_padded[1:0]; 
 * assign stage_enc[0][0] = input_padded[1];
 * assign stage_valid[0][1] = |input_padded[2:2];
 * assign stage_enc[0][1] = input_padded[3];
 * assign stage_valid[0][2] = |input_padded[5:4];
 * assign stage_enc[0][2] = input_padded[5];
 * assign stage_valid[0][3] = |input_padded[7:6];
 * assign stage_enc[0][3] = input_padded[7];
 *
 * // l = 1
 * assign stage_valid[1][0] = |stage_valid[0][1:0]; // l = 1; n = 0
 * assign stage_enc[1][1:0] = stage_valid[0][1] ? {1'b1, stage_enc[0][1:1]} : {1'b0, stage_enc[0][0:0]};  // l = 1; n = 0
 * assign stage_valid[1][1] = |stage_valid[0][3:2]; // l = 1; n = 1
 * assign stage_enc[1][3:2] = stage_valid[0][3] ? {1'b1, stage_enc[0][3:3]} : {1'b0, stage_enc[0][2:2]};  // l = 1; n = 1
 *
 * // l = 2
 * assign stage_valid[2][0] = |stage_valid[1][1:0];  // l = 2; n = 0
 * assign stage_enc[2][2:0] = stage_valid[1][1] ? {1'b1, stage_enc[1][3:2]} : {1'b0, stage_enc[1][1:0]};  // l = 2; n = 0
 *
 * 
 * assign o_valid = stage_valid[2];
 * assign o_encoded = stage_enc[2];
 * assign o_unencoded = 1 << o_encoded; 
 */

// the "macro" to dump signals
`ifdef COCOTB_SIM
initial begin
  $dumpfile ("priority_encoder.vcd");
  $dumpvars (0, priority_encoder);
  #1;
end
`endif

endmodule