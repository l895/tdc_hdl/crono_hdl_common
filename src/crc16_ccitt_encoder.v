/*
 * Verilog code for CRC16-CCITT calculator that uses a Linear Feedback Shift Register
 * in Galois Style.
 *
 * The CRC16-CCITT polynomial (called Kermit poly too) is: 0x1021
 *      p_kermit(x) = x^16 + x^12 + x^5 + 1
 *
 * In this module, an LFSR does the polynomial division of the CRC:
 * ,--------------------------+----------------------------------------------+----------------------------------+
 * |                          |                                              |                                  |
 * | .--.  .--.  .--.  .--.   V   .--.  .--.  .--.  .--.  .--.  .--.  .--.   V   .--.  .--.  .--.  .--.  .--.   V   
 * `-|15|<-|14|<-|13|<-|12|<-(+)<-|11|<-|10|<-| 9|<-| 8|<-| 7|<-| 6|<-| 5|<-(+)<-| 4|<-| 3|<-| 2|<-| 1|<-| 0|<-(+)<- DIN (MSB first)
 *   '--'  '--'  '--'  '--'       '--'  '--'  '--'  '--'  '--'  '--'  '--'       '--'  '--'  '--'  '--'  '--'       
 * An there is an Finite State Machine that controls the LFSR to interact with an external logic.
 * To use this module, following sequence must be done by an external logic:
 * 1) Set i_data_valid port to 1
 * 2) Wait until next clock cycle (here the fsm will reset the LFSR and will prepare the of the internal signals)
 * 3) Insert to the encoder the data bit by bit, starting from MSB and waiting 1 clock cycle between each bit
 * 4) whenever the LSB is inserted set i_data_valid port to 0
 * 5) Wait until o_crc_valid port is 1
 * 6) Read data from o_crc port
 */

`timescale 1ns / 1ps

module crc16_ccitt_encoder(i_data, i_data_valid, i_rst, clk, o_crc, o_crc_valid);
    input i_data;       // input data (serial form, must change clock cycle to clock cycle)
    input i_data_valid; // when is 1, the encoder will start the computation of the CRC  
    input i_rst;        // reset signal
    input clk;          // clock signal
    output [15:0] o_crc; // crc computation
    output o_crc_valid; // indicates when the crc_computation is ready to read

    /*- Linear Feedback Shift Register --------------------------------------*/
    // build the LFSR with the correspondent flip flops
    wire [15:0] w_lfsr_chain;
    wire w_lfsr_reset, w_lfsr_input;
    reg r_fsm_reset_lfsr;
    assign w_lfsr_reset = i_rst | r_fsm_reset_lfsr; 

    // we delayed the input of the lfsr one clock cycle to synchronize with the 
    // fsm that controls the module
    wire w_delayed_data_input, w_delayed_data_valid;
    ffd ffd_delayed_input  ( .D(i_data), .clk(clk), .sync_reset(i_rst),  .Q(w_delayed_data_input)  );
    ffd ffd_w_delayed_data_valid ( .D(i_data_valid), .clk(clk), .sync_reset(i_rst), .Q(w_delayed_data_valid) );
    
    // here we are selecting the input of the lfsr and also xoring with the last ffd of the chain
    wire w_input_mux;
    assign w_input_mux = (w_delayed_data_valid == 1) ? w_delayed_data_input: 0;
    assign w_lfsr_input = w_input_mux ^ w_lfsr_chain[15]; 

    ffd ffd0  ( .D(w_lfsr_input), .clk(clk), .sync_reset(w_lfsr_reset),  .Q(w_lfsr_chain[0])  );
    ffd ffd1  ( .D(w_lfsr_chain[0]), .clk(clk), .sync_reset(w_lfsr_reset),  .Q(w_lfsr_chain[1])  );
    ffd ffd2  ( .D(w_lfsr_chain[1]), .clk(clk), .sync_reset(w_lfsr_reset),  .Q(w_lfsr_chain[2])  );
    ffd ffd3  ( .D(w_lfsr_chain[2]), .clk(clk), .sync_reset(w_lfsr_reset),  .Q(w_lfsr_chain[3])  );
    ffd ffd4  ( .D(w_lfsr_chain[3]), .clk(clk), .sync_reset(w_lfsr_reset),  .Q(w_lfsr_chain[4])  );
    ffd ffd5  ( .D(w_lfsr_chain[4] ^ w_lfsr_chain[15]), .clk(clk), .sync_reset(w_lfsr_reset), .Q(w_lfsr_chain[5]) );
    ffd ffd6  ( .D(w_lfsr_chain[5]), .clk(clk), .sync_reset(w_lfsr_reset),  .Q(w_lfsr_chain[6])  );
    ffd ffd7  ( .D(w_lfsr_chain[6]), .clk(clk), .sync_reset(w_lfsr_reset),  .Q(w_lfsr_chain[7])  );
    ffd ffd8  ( .D(w_lfsr_chain[7]), .clk(clk), .sync_reset(w_lfsr_reset),  .Q(w_lfsr_chain[8])  );
    ffd ffd9  ( .D(w_lfsr_chain[8]), .clk(clk), .sync_reset(w_lfsr_reset),  .Q(w_lfsr_chain[9]) );
    ffd ffd10 ( .D(w_lfsr_chain[9]), .clk(clk), .sync_reset(w_lfsr_reset), .Q(w_lfsr_chain[10]) );
    ffd ffd11 ( .D(w_lfsr_chain[10]), .clk(clk), .sync_reset(w_lfsr_reset), .Q(w_lfsr_chain[11]) );
    ffd ffd12 ( .D(w_lfsr_chain[11] ^ w_lfsr_chain[15]), .clk(clk), .sync_reset(w_lfsr_reset), .Q(w_lfsr_chain[12]) );
    ffd ffd13 ( .D(w_lfsr_chain[12]), .clk(clk), .sync_reset(w_lfsr_reset), .Q(w_lfsr_chain[13]) );
    ffd ffd14 ( .D(w_lfsr_chain[13]), .clk(clk), .sync_reset(w_lfsr_reset), .Q(w_lfsr_chain[14]) );
    ffd ffd15 ( .D(w_lfsr_chain[14]), .clk(clk), .sync_reset(w_lfsr_reset), .Q(w_lfsr_chain[15]) );

    /*- Clock cycles counter ------------------------------------------------*/
    reg[4:0] r_clock_cycles_counter; // used to count clock cycles to wait until encoding is finishe
    reg r_fsm_reset_clock_cycles_counter, r_clock_cycles_counter_finished;
    wire w_clock_cycles_counter_reset;
    assign w_clock_cycles_counter_reset = r_fsm_reset_clock_cycles_counter | i_rst;

    // Clock cycles counter
    always @(posedge clk) begin
        if( w_clock_cycles_counter_reset == 1 ) begin
            r_clock_cycles_counter <= 0; 
            r_clock_cycles_counter_finished <= 0;
        end else if(r_clock_cycles_counter >= 14) begin
            r_clock_cycles_counter_finished <= 1;
        end else begin 
            r_clock_cycles_counter <= r_clock_cycles_counter + 1;
        end
    end

    /*- Finite State Machine ------------------------------------------------*/
    // state machine definition
    localparam  state_idle                          = 2'b00,
                state_encoding                      = 2'b01,
                state_wait_until_encoding_finishes  = 2'b10,
                state_encoding_finished             = 2'b11;

    reg[1:0] fsm_state, fsm_next_state;

    // Current state register (sequential logic)
    always @(posedge clk) begin
        if( i_rst == 1'b1 ) begin
          fsm_state <= state_idle;
        end else begin // otherwise update the states
          fsm_state <= fsm_next_state;
        end
    end

    always @(fsm_state, i_data_valid, r_clock_cycles_counter_finished) begin
        // Set undefined as default state, is good to catch bugs if
        // a transitions is not defined!
        fsm_next_state <= 2'bxx;

        // Decide next state based on input (enable signal)
        case(fsm_state)
            state_idle:
                begin
                    if( i_data_valid == 1'b1 ) begin
                        fsm_next_state <= state_encoding;
                    end else begin
                        fsm_next_state <= state_idle;
                    end
                end

            state_encoding:
                begin
                    if( i_data_valid == 1'b0 ) begin
                        fsm_next_state <= state_wait_until_encoding_finishes;
                    end else begin
                        fsm_next_state <= state_encoding;
                    end
                end

            state_wait_until_encoding_finishes:
                begin
                    if( r_clock_cycles_counter_finished == 1 ) begin
                        fsm_next_state <= state_encoding_finished;
                    end else begin
                        fsm_next_state <= state_wait_until_encoding_finishes;
                    end
                end

            // just be here one clock cycle
            state_encoding_finished:
                begin
                    fsm_next_state <= state_idle;
                end
        endcase
    end

    // Combinational output logic
    reg r_sample_lfsr_chain;
    
    always @(fsm_state) begin
        r_sample_lfsr_chain <= 0;
        r_fsm_reset_lfsr <= 1; // mantain lfsr in reset state
        r_fsm_reset_clock_cycles_counter <= 1;

        case(fsm_state)

            state_encoding:
                begin
                    r_fsm_reset_lfsr <= 0; // unreset lfsr
                end
                
            state_wait_until_encoding_finishes:
                begin
                    r_fsm_reset_lfsr <= 0; // unreset lfsr
                    r_fsm_reset_clock_cycles_counter <= 0;
                end
            
            state_encoding_finished:
                begin
                    r_sample_lfsr_chain <= 1;
                end
        endcase
    end

    /*- Build output values -------------------------------------------------*/
    assign o_crc = w_lfsr_chain;
    assign o_crc_valid = r_sample_lfsr_chain; 
//    ffd o_crc_valid_ffd ( .D(r_sample_lfsr_chain), .clk(clk), .sync_reset(i_rst), .Q(o_crc_valid) );
//
//    always @(posedge clk) begin
//        if( i_rst == 1 ) begin
//            o_crc <= 0;
//        end if( r_sample_lfsr_chain == 1 ) begin
//            o_crc <= w_lfsr_chain;
//        end
//   end

// the "macro" to dump signals
`ifdef COCOTB_SIM
initial begin
  $dumpfile ("crc16_ccitt_encoder.vcd");
  $dumpvars (0, crc16_ccitt_encoder);
  #1;
end
`endif

endmodule