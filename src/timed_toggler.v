/*
 * Verilog code of a circuit that toggles an output signal every N milliseconds.
 * Author: Julian Rodriguez <jnrodriguez@estudiantes.unsam.edu.ar>
 */

//`timescale 1us / 1us

module timed_toggler(i_clk, i_reset, o_toggle_out);
    parameter TOGGLE_PERIOD_MS = 1000;     // toggling period in milliseconds
    parameter CLOCK_FREQUENCY_HZ = 100000000;  // clock frequency in Hz
    input i_clk, i_reset;
    output reg o_toggle_out;

    integer TOGGLE_EVERY_COUNTS;
    integer counter;

    initial begin
        TOGGLE_EVERY_COUNTS = (TOGGLE_PERIOD_MS < 1000) ? (TOGGLE_PERIOD_MS*CLOCK_FREQUENCY_HZ)/1000 : (TOGGLE_PERIOD_MS/1000)*CLOCK_FREQUENCY_HZ;
    end

    always @(posedge i_clk) begin
        if( i_reset == 1'b1 ) begin
            counter <= 0;
            o_toggle_out <= 0;
        end else if( counter > TOGGLE_EVERY_COUNTS ) begin
            counter <= 0;
            o_toggle_out <= ~o_toggle_out;
        end else begin
            counter <= counter + 1;
            o_toggle_out <= o_toggle_out;
        end
    end

// the "macro" to dump signals
`ifdef COCOTB_SIM
initial begin
  $dumpfile ("timed_toggler.vcd");
  $dumpvars (0, timed_toggler);
  #1;
end

`else

    /* ILA instantiation if we are debugging this module with Vivado */
    //`define DEBUG_TIMED_TOGGLER 1
    `ifdef DEBUG_TIMED_TOGGLER
    ila_0 ila_timed_toggler (
        .clk(i_clk), // input wire clk
        .probe0(i_reset),     // 1bit
        .probe1(counter),     // reg[31:0] 
        .probe2(o_toggle_out) // 1bit
    );
    `endif

`endif

endmodule  