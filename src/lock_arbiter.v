/*
 * Verilog code to implement a lock arbiter with priorities.
 *
 * This module decides which of the N inputs will have access to a shared
 * resource.
 * If multiple requesters wants to use the resource, there is priorities,
 * and the requester that holds a most significative bit of the input will take
 * the resource. To implement this, the module uses a priority encoder.
 *
 *
 * Author: Julian Rodriguez <jnrodriguez@estudiantes.unsam.edu.ar>
 */

module lock_arbiter (i_clk, i_reset, i_requesters, o_grants_encoded, o_grants_unencoded);
    parameter REQUESTERS_QTY = 6;   // quantity of requesters that will want to use the shared resource
    input       i_clk;              // input clock
    input       i_reset;
    input [REQUESTERS_QTY-1:0] i_requesters; // requesters input port, the rightmost bit will have more priority
    output reg [REQUESTERS_QTY-1:0] o_grants_unencoded; // grants unencoded port, notifies which is the requester that can use the shared resource in one-hot encoding 
    output reg [$clog2(REQUESTERS_QTY)-1:0] o_grants_encoded; // grants encoded port, notifies which is the requester that can use the shared resource in a binary representation

    // Instantiate priority encoder
    wire encoder_out_valid;
    wire [$clog2(REQUESTERS_QTY)-1:0] requesters_encoded;
    wire [REQUESTERS_QTY-1:0] requesters_unencoded;

    priority_encoder #(.WIDTH(REQUESTERS_QTY)) encoder (
        .i_unencoded    (i_requesters),
        .o_valid        (encoder_out_valid),
        .o_encoded      (requesters_encoded),
        .o_unencoded    (requesters_unencoded)
    );

    localparam  state_resource_released = 1'b0,
                state_resource_locked   = 1'b1;

    reg fsm_state, fsm_next_state;

    // Current state register (sequential logic)
    always @(posedge i_clk) begin
        if( i_reset == 1'b1 ) begin
            fsm_state <= state_resource_released;
        end else begin // otherwise update the states
            fsm_state <= fsm_next_state;
        end
    end

    always @(fsm_state, encoder_out_valid, grants_finished) begin
        // Set undefined as default state, is good to catch bugs if
        // a transitions is not defined!
        fsm_next_state <= 1'bx;

        // Decide next state based on input
        case(fsm_state)
            state_resource_released:
                begin
                    if( encoder_out_valid != 0 ) begin
                        fsm_next_state <= state_resource_locked;
                    end else begin
                        fsm_next_state <= state_resource_released;
                    end
                end

            state_resource_locked:
                begin
                    if( grants_finished == 1 ) begin
                        fsm_next_state <= state_resource_released;
                    end else begin
                        fsm_next_state <= state_resource_locked;
                    end
                end
        endcase
    end

    // Combinational output logic: will change only when fsm state changes
    always @(fsm_state, requesters_encoded, requesters_unencoded) begin
        o_grants_encoded <= 0;
        o_grants_unencoded <= 0;

        case(fsm_state)
            state_resource_locked:
                begin
                    o_grants_encoded <= requesters_encoded;
                    o_grants_unencoded <= requesters_unencoded;
                end
        endcase
    end

wire [REQUESTERS_QTY-1:0] grants_finished;
assign grants_finished = ~(i_requesters >> o_grants_encoded);


// the "macro" to dump signals
`ifdef COCOTB_SIM
initial begin
  $dumpfile ("lock_arbiter.vcd");
  $dumpvars (0, lock_arbiter);
  #1;
end
`endif

endmodule