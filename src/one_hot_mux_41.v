/*
 * Verilog code of a one-hot 4:1 multiplexer where each input have
 * INPUT_WIDTH bits.
 * The difference between this mux and a normal mux is that the select
 * signal is in one-hot encoding. So, if select is equal to:
 * b0001 -> input 0 it is selected
 * b0010 -> input 1 it is selected
 * b0100 -> input 2 it is selected
 * b1000 -> input 3 it is selected
 * In any other case the output is high-z
 * Author: Julian Rodriguez <jnrodriguez@estudiantes.unsam.edu.ar>
 */

module one_hot_mux_41(i_data0,i_data1,i_data2,i_data3,o_data,i_sel);
    parameter INPUT_WIDTH = 19;     // width of each input (bits)

    input [3:0] i_sel; // selector port
    input [INPUT_WIDTH-1:0] i_data0;
    input [INPUT_WIDTH-1:0] i_data1;
    input [INPUT_WIDTH-1:0] i_data2;
    input [INPUT_WIDTH-1:0] i_data3;
    output reg [INPUT_WIDTH-1:0] o_data;   // data output 

        always @(*) begin
            case(i_sel)
                4'b0001: o_data <= i_data0;
                4'b0010: o_data <= i_data1;
                4'b0100: o_data <= i_data2;
                4'b1000: o_data <= i_data3;
                default: o_data <= {INPUT_WIDTH{1'bz}};
            endcase
        end

// the "macro" to dump signals
`ifdef COCOTB_SIM
initial begin
  $dumpfile ("one_hot_mux_41.vcd");
  $dumpvars (0, one_hot_mux_41);
  #1;
end
`endif

endmodule 