/*
 * Verilog code for register of N bits with synchronous reset input
 * Author: Julian Rodriguez <jnrodriguez@estudiantes.unsam.edu.ar>
 */

module reg_n(D,clk,sync_reset,Q);
    parameter N = 9;    // quantity of bits of the register
    parameter RESET_VALUE = 0; // reset value

    input sync_reset;   // synchronous reset 
    input clk;          // clock input 
    
    input [N-1:0] D; // Data input 
    output reg [N-1:0] Q; // Data output 

always @(posedge clk) 
    begin
        if(sync_reset==1'b1)
            Q <= RESET_VALUE;
        else 
            Q <= D; 
        end

// the "macro" to dump signals
`ifdef COCOTB_SIM
initial begin
  $dumpfile ("reg_n.vcd");
  $dumpvars (0, reg_n);
  #1;
end
`endif

endmodule 