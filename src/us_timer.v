/*
 * Verilog code for programmable microsecond timer.
 *
 * This module implements a timer prepared to count microseconds. Can count up to
 * 2^32-1 us = 4294967295 us ~= 4295 s.
 * This time can be programmed through ports `i_time_to_count_us` and `i_time_to_count_we`.
 * And once the time is reached the port `o_time_reached` is set until the next reset.
 * The module is prepared to be feeded with a MHz clock, so, it will count up to
 * N times the value of the parameter `CLOCK_FREQUENCY_MHz`.
 *
 * To use the timer:
 * 1) Set the microseconds that must be counted setting a 1 to the `i_time_to_count_we` port and
 * writing the microseconds in the port `i_time_to_count_us`. Then wait a clock cycle to the 
 * setting take effect.
 * 2) Reset the timer, setting the reset port `i_reset` high during a clock cycle.
 * 3) Wait until the port `o_time_reached` is set!
 *
 * Author: Julian Rodriguez <jnrodriguez@estudiantes.unsam.edu.ar>
 */

`timescale 1us / 1ns

module us_timer(i_clk,i_reset,i_time_to_count_we,i_time_to_count_us,o_time_reached);
    parameter INITIAL_TIME_TO_COUNT_US = 100; /* Initial value of the microseconds to count. */
    parameter CLOCK_FREQUENCY_MHZ = 100;      /* Must be filled with the frequency of the clock (in MHz). */

    input i_clk;                // input clock
    input i_reset;              // synchronous input reset
    input [31:0] i_time_to_count_us; // input to set the microseconds to count
    input i_time_to_count_we;   // signal that must be activated to write the max_count value
    output reg o_time_reached;  // signal activates when the time is reached

    integer r_clock_cycles_count = 0;   /* Will hold the quantity of clock cycles counted until the moment. */
    integer r_us_count = 0;             /* Will hold the quantity of microseconds counted until the momment.*/
    integer r_us_time_to_count = INITIAL_TIME_TO_COUNT_US; /* Will hold the quantity of microseconds to count. */

    always @(posedge i_clk)
        if( i_reset == 1 ) begin
            /* Must start from -1 because will count from 0 to CLOCK_FREQUENCY_MHZ-1.
             * The first clock cycle after reset the counter will start from 0 if the reset value is -1.
             */
            r_clock_cycles_count <= -1;
            r_us_count <= 0;
        end else if( i_time_to_count_we == 1 ) begin
            r_us_time_to_count <= i_time_to_count_us;
        end else begin
            if( r_clock_cycles_count < CLOCK_FREQUENCY_MHZ-1 ) begin
                r_clock_cycles_count <= r_clock_cycles_count + 1;
            end else begin
                r_clock_cycles_count <= 0;
                r_us_count <= r_us_count + 1;
            end
        end


    always @* begin
        if( i_reset == 1 ) begin
            o_time_reached <= 0;
        end else if( r_us_count >= r_us_time_to_count ) begin
            o_time_reached <= 1;
        end else begin
            o_time_reached <= 0;
        end
    end


// the "macro" to dump signals
`ifdef COCOTB_SIM
initial begin
  $dumpfile ("us_timer.vcd");
  $dumpvars (0, us_timer);
  #1;
end
`endif

endmodule 