/*
 * Single-Port BRAM with Byte-wide Write Enable
 * NB_COLxCOL_WIDTH-bit write
 * Read-First mode
 * Single-process description
 * Compact description of the write with a generate-for statement
 * Column width and number of columns easily configurable
 *
 * Notes from Julian:
 * The RAM is declared as a 2D array. You have SIZE rows of NB_COL*COL_WIDTH
 * bits.
 * The 'generate' sentence creates NB_COL processes, for example, with NB_COL=3 and COL_WIDTH=7
 * we are creating 3 always processes:
 *
 *  i = 0 || RAM[addr][(0+1)*7-1:0*7] <= di[(0+1)*7-1:0*7] || RAM[addr][6:0] <= di[6:0]
 *  always @(posedge clk) begin
 *      if (we[0])
 *          RAM[addr][6:0] <= di[6:0]
 *  end
 *
 *  i = 1 || RAM[addr][(1+1)*7-1:1*7] <= di[(1+1)*7-1:1*7] || RAM[addr][13:7] <= di[13:7]
 *  always @(posedge clk) begin
 *      if (we[1])
 *          RAM[addr][13:7] <= di[13:7]
 *  end
 *
 *  i = 2 || RAM[addr][(2+1)*7-1:2*7] <= di[(2+1)*7-1:2*7] || RAM[addr][20:14] <= di[20:14]
 *  always @(posedge clk) begin
 *      if (we[2])
 *          RAM[addr][20:14] <= di[20:14]
 *  end
 *
 * The `address` corresponds to the row of the RAM. In each cycle, the RAM checks if the
 * the `write enable` of a column is asserted. If it is asserted, it takes the bits that 
 * corresponds to that column from `di` (data input) and stores them in the row selected
 * by the `address`.
 * In this manner, we can write a column independently.
 *
 * Download: http://www.xilinx.com/txpatches/pub/documentation/misc/xstug_examples.zip
 * File: HDL_Coding_Techniques/rams/bytewrite_ram_1b.v
 */


module ram_read_first(clk, we, addr, di, dout);
    parameter SIZE = 8192;      // Quantity of rows
    parameter ADDR_WIDTH = 10;  // Width of the address register
    parameter COL_WIDTH = 6;    // Width of each column
    parameter NB_COL = 3;       // Quantity of columns in a row

    input clk;
    input [NB_COL-1:0] we;                      // write enable
    input [ADDR_WIDTH-1:0] addr;                // address to read/write (we must to index up to SIZE rows)
    input [NB_COL*COL_WIDTH-1:0] di;            // data to write into the RAM
    output reg [NB_COL*COL_WIDTH-1:0] dout;       // data readed from the RAM

    reg [NB_COL*COL_WIDTH-1:0] RAM [SIZE-1:0]; // the RAM buffer

    // In the read operations, all the row that belongs to a certain address is readed
    always @(posedge clk) begin
        dout <= RAM[addr];
    end

    // Create NB_COL always processes used to write independently in each column of the RAM    
    genvar i;
    generate
        for (i = 0; i < NB_COL; i = i+1)
        begin
            always @(posedge clk)
            begin
                if (we[i])
                    RAM[addr][(i+1)*COL_WIDTH-1:i*COL_WIDTH] <= di[(i+1)*COL_WIDTH-1:i*COL_WIDTH];
            end
        end
    endgenerate

// the "macro" to dump signals
`ifdef COCOTB_SIM
initial begin
  $dumpfile ("ram_read_first.vcd");
  $dumpvars (0, ram_read_first);
  #1;
end
`endif

endmodule
